module SysJournal (getTodayContent) where

import Data.Time.LocalTime
import Data.Time.Clock.System
import Data.Time.Format
import Data.Time.Clock
import System.Environment
import System.IO

getTodayContent :: IO [String]
getTodayContent =   do
        tm <- getCurrentTimeZone
        localTime <- getCurrentTime
        let modified = utcToLocalTime tm  localTime 
        --currentTime <- systemtoutctime <$>  getsystemtime
        let formattedtime = formatTime defaultTimeLocale "%h:%m:%s" modified
        let fp = formatTime defaultTimeLocale "%Y-%m-%d.org" modified
        lines <$> readFile fp 

main = do
    args <- getArgs
    case args of
        [content] -> do 
--           putstrln $ show $ formattime defaulttimelocale "%h:%m:%s" modified
            processTextFile content
            totalContent <- getTodayContent
            mapM_ putStrLn totalContent 
        _ -> putStrLn "Usage: journal content"

processTextFile ::  String -> IO()
processTextFile textLine = do 
        tm <- getCurrentTimeZone
        localTime <- getCurrentTime
        let modified = utcToLocalTime tm  localTime 
        --currentTime <- systemToUTCTime <$>  getSystemTime
        let formattedTime = formatTime defaultTimeLocale "%H:%M:%S" modified
        let fp = formatTime defaultTimeLocale "%Y-%m-%d.org" modified
        appendFile fp ( formattedTime <> " -- "   <> addNewLine textLine )

addNewLine :: String -> String
addNewLine str = str ++ "\n"