{-#Language NoImplicitPrelude#-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}

import SysJournal
import qualified Data.Text as Text
import Servant
import Relude

import Network.Wai.Middleware.Cors 
import Network.Wai.Handler.Warp
import qualified Network.HTTP.Types as HTTP

data DB = DB
  { notes :: TVar [Note]
--    bookings :: TVar [Booking],
--    lastBookingIdx :: TVar Int
  }

type AppM = ReaderT DB Handler
type Note = String
--type Tasks = [Task]

getTasks ::  AppM [Note]
getTasks = do
  DB {notes = p} <- ask
  liftIO $ readTVarIO p

server :: ServerT JournalAPI AppM
-- server = createTask  :<|> getTasks  
server = getTasks 

type JournalAPI = GetAPI 

userAPI :: Proxy JournalAPI
userAPI = Proxy

type GetAPI = "todos" :> Get '[JSON] [Note]

nt :: DB -> AppM a -> Handler a
nt s x = runReaderT x s
--mySimpleCors 
app :: DB -> Application
app s = mySimpleCors $ serve userAPI $ hoistServer userAPI (nt s) server

mySimpleCors = cors $ const $ Just simpleCorsResourcePolicy {
   -- corsRequestHeaders = ["Authorization", "Content-Type"]
  -- corsOrigins = Just (["http://localhost:8000"],  True)
  corsMethods = ["OPTIONS", "GET", "PATCH", "PUT", "POST"]
  ,corsRequestHeaders =  ["Content-Type"]
    } -- :: [HTTP.HeaderName])
-- "Authorization", 

main :: IO ()
main = do
  putStrLn "running..."
  initialContent <- getTodayContent
--  initialTodos <- newTVarIO [Todo "watch porn" False 1]
  initialContentTVar <- newTVarIO initialContent
  run 8081 $ app $ DB initialContentTVar
  putStrLn "...closed!!"
