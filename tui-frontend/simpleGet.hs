{-#  LANGUAGE OverloadedStrings #-}

import Prelude hiding (id)
import Network.HTTP.Conduit
import Network.HTTP.Simple
import Data.Aeson
import Data.Aeson.Types

-- Define a data type to represent your JSON structure
data TodoJSON = TodoJSON { id :: Int, status :: Bool, task :: String } deriving (Show)

instance FromJSON TodoJSON where
    parseJSON = withObject "TodoJSON" $ \o -> do
        rId <- o .: "id"
        rStatus <- o .: "status"
        rTask <- o .: "task"
        return TodoJSON {id = rId, status = rStatus, task = rTask}

main = do
    let url = "http://127.0.0.1:5000/todos"
    request <- parseRequest url

    response <- httpLBS request

    putStrLn $ "Response status code: " <> show (getResponseStatus response)
    putStrLn $ "Response body:"
    putStrLn $ show $ getResponseBody response 
        -- "http://"
    putStrLn "emacs"