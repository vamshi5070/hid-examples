{-#  LANGUAGE OverloadedStrings #-}

import Prelude hiding (id)
import qualified Data.ByteString.Lazy as L
import Network.HTTP.Conduit
import Network.HTTP.Simple
import Data.Aeson
import Control.Exception (try,SomeException)
import Data.Aeson.Types

import Brick
import Brick.Widgets.Core 
import qualified Brick.Widgets.Border.Style as BS
import qualified Brick.Widgets.Center as C
import qualified Brick.Widgets.Border as B

-- Define a data type to represent your JSON structure
data TodoJSON = TodoJSON { id :: Int, status :: Bool, task :: String } deriving (Show)

instance FromJSON TodoJSON where
    parseJSON = withObject "TodoJSON" $ \o -> do
        rId <- o .: "id"
        rStatus <- o .: "status"
        rTask <- o .: "task"
        return TodoJSON {id = rId, status = rStatus, task = rTask}

drawShowable :: (Show a) => a -> Widget ()
drawShowable num = withBorderStyle BS.unicodeBold 
    $ B.border
    $ C.hCenter
    $ padAll 1
    $ str $ show num

drawStr :: String -> Widget ()
drawStr myStr = withBorderStyle BS.unicodeBold 
    $ B.border
    $ C.hCenter
    $ padAll 1
    $ str $ show myStr

drawUI :: [TodoJSON] -> IO ()
drawUI todos = simpleMain 
                $ withBorderStyle BS.unicodeBold 
                $ B.borderWithLabel (str "Todos")
                -- $ (str "eamcs" :: Widget ())
                $ ((vBox $ map (\(TodoJSON myId myStatus myTask) -> 
                    hBox $ [drawShowable myId,drawShowable myStatus,drawStr myTask]) todos ) :: Widget ())
        -- $ vBox $ map drawInt xs 

main :: IO()
main = do
    let url = "http://127.0.0.1:5000/todos"
    request <- parseRequest url

    response <- try $ httpLBS request :: IO (Either SomeException (Response L.ByteString))
    -- LByteString))

    case response of
        Left e -> putStrLn $ "Failed to fetch data" <> show e
        Right r -> case (decode (getResponseBody r))  :: Maybe [TodoJSON] of
            Just dataList -> do
                putStrLn "Received and parsed JSON data:"
                drawUI dataList
                -- drawUI $ map (\(TodoJSON myId _ _) -> myId) dataList
            Nothing -> putStrLn "Failed in understanding json"

    -- putStrLn $ "Response status code: " <> show (getResponseStatus response)
    -- putStrLn $ "Response body:"
    -- putStrLn $ show $ getResponseBody response 
        -- "http://"
    putStrLn "emacs"

ui :: Widget ()
ui = withBorderStyle BS.unicodeBold 
    $ B.border
    $ C.hCenter
    $ padAll 1
    $ str $ show 1
    -- $ vBox [str "Hello, world!", str "Rammana choudary"]

-- myUI :: Int -> Widget ()
-- myUI num = withBorderStyle BS.unicodeBold 
--     $ B.border
--     $ C.hCenter
--     $ padAll 1
--     $ str $ show num

-- main :: IO ()
-- main = simpleMain 
--         $ withBorderStyle BS.unicodeBold 
--         $ B.borderWithLabel (str "Linear search")
--         $ hBox $ map (\(index,value) -> vBox [(myUI value ),(C.hCenter $ str $ show index)]) 
--         $ zip [0..] [83,123,226,248]