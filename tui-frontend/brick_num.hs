import Brick
import Brick.Widgets.Core 
import qualified Brick.Widgets.Border.Style as BS
import qualified Brick.Widgets.Center as C
import qualified Brick.Widgets.Border as B


ui :: Widget ()
ui = withBorderStyle BS.unicodeBold 
    $ B.border
    $ C.hCenter
    $ padAll 1
    $ str $ show 1
    -- $ vBox [str "Hello, world!", str "Rammana choudary"]

myUI :: Int -> Widget ()
myUI num = withBorderStyle BS.unicodeBold 
    $ B.border
    $ C.hCenter
    $ padAll 1
    $ str $ show num

main :: IO ()
main = simpleMain 
        $ withBorderStyle BS.unicodeBold 
        $ B.borderWithLabel (str "Linear search")
        $ hBox $ map (\(index,value) -> vBox [(myUI value ),(C.hCenter $ str $ show index)]) $ zip [0..] [83,123,226,248]